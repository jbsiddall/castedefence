package game;

public class EntityStateView
{
	private String type;
	private int x;
	private int y;
	private Direction direction;
	private int depth;

	public EntityStateView(Entity e)
	{
		type = e.getType();
		x = e.getX();
		y = e.getY();
		direction = e.getDirection();
		depth = e.getDepth();
	}

	public String getType()
	{
		return type;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public Direction getDirection()
	{
		return direction;
	}

	public int getDepth()
	{
		return depth;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o) return true;
		if(!(o instanceof EntityStateView)) return false;

		EntityStateView that = (EntityStateView) o;

		if(depth != that.depth) return false;
		if(x != that.x) return false;
		if(y != that.y) return false;
		if(direction != that.direction) return false;
		if(type != null ? !type.equals(that.type) : that.type != null) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = type != null ? type.hashCode() : 0;
		result = 31 * result + x;
		result = 31 * result + y;
		result = 31 * result + (direction != null ? direction.hashCode() : 0);
		result = 31 * result + depth;
		return result;
	}

	@Override
	public String toString()
	{
		return "EntityStateView{" +
					   "type='" + type + '\'' +
					   ", x=" + x +
					   ", y=" + y +
					   ", direction=" + direction +
					   ", depth=" + depth +
					   '}';
	}
}
