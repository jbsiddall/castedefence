package game.entity;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.name.Named;
import game.*;

import java.util.LinkedList;
import java.util.List;

public class OrcSpawner extends Entity
{
	private int mana;
	private int orcManaCost;
	private int spawnRate;
	private List<Point> spawnPoints;

	@Inject
	public OrcSpawner
	(
			@Assisted("id") long id,
			@Assisted("x") int x,
			@Assisted("y") int y,
			@Assisted("direction") Direction direction,
			@Named("worldWidth") int worldWidth,
			@Named("worldHeight") int worldHeight
	)
	{
		super(id, x, y, direction, 1);
		mana = 0;
		orcManaCost = 1;
		spawnRate = 2;
		spawnPoints = new LinkedList<Point>();
		spawnPoints.add(new Point(x,y));
		spawnPoints.add(new Point(x-1,y));
		spawnPoints.add(new Point(x+1,y));
		spawnPoints.add(new Point(x,y-1));
		spawnPoints.add(new Point(x,y+1));
	}

	@Override
	public void update(WorldView world)
	{
		if(world.getStage() == TimeStage.Defend)
		{
			int spawned = 0;
			while(spawned < spawnRate & mana > orcManaCost)
			{
				List<Point> validPoints = new LinkedList<Point>();
				for(Point p : spawnPoints)
				{
					if(world.isInsideBoard(p.getX(),p.getY()) && world.getEntitiesByQuery(Query.Location, p, Query.Solid, true).isEmpty())
					{
						validPoints.add(p);
					}
				}
				int spawnPointIndex = (int)(Math.random() * validPoints.size());
				Point spawnPoint = validPoints.get(spawnPointIndex);

				if(world.getEntitiesByQuery(Query.Location, spawnPoint, Query.Solid, true).isEmpty())
				{
					spawned++;
					mana -= orcManaCost;
					world.createEntity(EntityType.Orc, spawnPoint.getX(), spawnPoint.getY(), Direction.North);
				}
			}
		}

	}

	@Override
	public String getType() { return EntityType.OrcSpawner; }

	@Override
	public int getDepth() { return 1; }

	@Override
	public Team getTeam() { return Team.Orc; }

	@Override
	public boolean isSolid() { return false; }

	@Override
	public boolean isDestroyable() { return false; }

	public void addMana(int deltaMana)
	{
		mana += deltaMana;
	}

}
