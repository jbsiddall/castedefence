package game.window;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import game.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;

public class GameScreen extends JPanel implements MouseListener, KeyListener
{

	private final int worldWidth;
	private final int worldHeight;

	private int viewPortX;
	private int viewPortY;
	private int viewPortWidth;
	private int viewPortHeight;

	private StateView gameState;
	private InteractionManager interactionManager;

	private Map<String, Color> entityTypeColors;

	@Inject
	public GameScreen
	(
			@Named("gameScreen") Size gameScreenSize,
			@Named("worldWidth") int worldWidth,
			@Named("worldHeight") int worldHeight,
			@Named("entityTypeColors") Map<String, Color> entityTypeColors,
			StateView initialGameState,
			InteractionManager interactionManager
	)
	{
		super();

		setPreferredSize(new Dimension(gameScreenSize.getWidth(), gameScreenSize.getHeight()));
		setSize(new Dimension(gameScreenSize.getWidth(), gameScreenSize.getHeight()));

		this.worldWidth = worldWidth;
		this.worldHeight = worldHeight;
		this.interactionManager = interactionManager;
		this.gameState = initialGameState;
		addMouseListener(this);
		addKeyListener(this);

		EntityStateView treasure = null;
		for(EntityStateView e : gameState.getEntities())
			if(e.getType() == EntityType.Treasure)
			{
				treasure = e;
				break;
			}

//		if(treasure == null)
//		{
			viewPortX = 1;
			viewPortY = 1;
//		}
//		else
//		{
//			viewPortX = treasure.getX()-5;
//			viewPortY = treasure.getY()-5;
//		}

		viewPortWidth = worldWidth;
		viewPortHeight = worldHeight;


		this.entityTypeColors = entityTypeColors;

	}

	public StateView getGameState()
	{
		synchronized (gameState)
		{
			return gameState;
		}
	}

	public void setGameState(StateView gameState)
	{
		synchronized (gameState)
		{
			this.gameState = gameState;
		}

		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				GameScreen.this.repaint();
			}
		});


	}

	public void scroll(int dx, int dy)
	{
		this.viewPortX = Math.max(1, this.viewPortX + dx);
		this.viewPortY = Math.max(1, this.viewPortY+dy);
	}


	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		Graphics2D graphics = (Graphics2D)g;
		StateView state = getGameState();

		EntityStateView[][] entities = state.getView(
			viewPortX,
			viewPortY,
			viewPortWidth,
			viewPortHeight
		);


		int squareWidth = (int)(((double)getWidth()) / viewPortWidth);
		int squareHeight = (int)(((double)getHeight()) / viewPortHeight);

		setBackground(Color.GREEN.darker());

		for(int rowIndex = 0; rowIndex < viewPortWidth; rowIndex++)
		{
			for(int colIndex = 0; colIndex < viewPortHeight; colIndex++)
			{
				EntityStateView entity = entities[rowIndex][colIndex];
				int x = colIndex * squareWidth;
				int y = rowIndex * squareHeight;

				if(entity!=null)
				{
					graphics.setColor(entityTypeColors.get(entity.getType()));
					graphics.fillRect(x, y, squareWidth, squareHeight);
				}

				graphics.setColor(Color.black);
				graphics.drawRect(x, y, squareWidth, squareHeight);

			}
		}
	}


	private game.Point getWorldPoint(int mouseX, int mouseY, int panelWidth, int panelHeight)
	{
		double scaleX = (double)worldWidth / panelWidth;
		double scaleY = (double)worldHeight / panelHeight;

		int worldX = (int)(mouseX * scaleX) + 1;
		int worldY = (int)((panelHeight - mouseY+1) * scaleY) + 1;

		return new game.Point(worldX, worldY);
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		if(gameState.getStage() == TimeStage.Build)
		{
			int x = e.getX();
			int y = e.getY();

			int width = e.getComponent().getWidth();
			int height = e.getComponent().getHeight();

			interactionManager.worldClick
			(
				   getWorldPoint(x, y, width, height),
				   MouseEvent.BUTTON1 == e.getButton()
			);
		}

	}


	@Override
	public void mousePressed(MouseEvent e)
	{
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		System.out.println("key typed: " + e.getKeyChar());
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		System.out.println("key pressed: " + e.getKeyChar());
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		//To change body of implemented methods use File | Settings | File Templates.
	}
}
