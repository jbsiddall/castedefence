package game;

public abstract class Entity
{
	private final long id;
	private int x;
	private int y;
	private Direction direction;
	private int health;

	public Entity
	(
			long id,
			int x,
			int y,
			Direction direction,
			int health
	)
	{
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.health = health;
	}

	public abstract void update(WorldView world);
	public abstract String getType();
	public abstract int getDepth();
	public abstract Team getTeam();
	public abstract boolean isSolid();
	public abstract boolean isDestroyable();

	public void setDirection(Direction direction)
	{
		this.direction = direction;
	}

	public void setLocation(int x, int y)
	{
		this.x = x;
		this.y = y;
	}


	public int getHealth()
	{
		return health;
	}


	public void damage(int damage)
	{
		if(isDestroyable())
			health -= Math.min(health, damage);
	}


	public long getId()
	{
		return id;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public Direction getDirection()
	{
		return direction;
	}


	public boolean isAlive()
	{
		return getHealth() > 0;
	}

	public Point getLocation()
	{
		return new Point(getX(), getY());
	}

	public double distance(int x, int y) { return distance(new Point(x,y)); }
	public double distance(Point loc)
	{
		int x1 = getX();
		int y1 = getY();
		int x2 = loc.getX();
		int y2 = loc.getY();

		return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o) return true;
		if(!(o instanceof Entity)) return false;

		Entity entity = (Entity) o;

		if(id != entity.id) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return (int) (id ^ (id >>> 32));
	}

	@Override
	public String toString()
	{
		return "Entity{" +
					   "id=" + id +
					   ", type=" + getType() +
					   ", x=" + x +
					   ", y=" + y +
					   ", direction=" + direction +
					   ", health=" + health +
					   '}';
	}
}
