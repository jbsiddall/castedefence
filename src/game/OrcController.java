package game;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import game.entity.OrcSpawner;

import java.util.Set;

public class OrcController implements Controller
{
	private WorldView world;
	private static final int INITIAL_WAVE_SIZE = 20;
	private static final double WAVE_INCREASE = 0.5;
	private int waveSize;
	private boolean spawned;

	@Inject
	public OrcController(@Assisted WorldView world)
	{
		this.world = world;
		spawned = false;
		waveSize = INITIAL_WAVE_SIZE;
	}

	public void update()
	{
		if(world.getStage() == TimeStage.Defend)
		{
			if(spawned == false)
			{
				spawned = true;
				waveSize += (int)(waveSize * WAVE_INCREASE);
				Set<Long> spawnPoints = world.getEntitiesByQuery(Query.Type, EntityType.OrcSpawner);
				for(Long id : spawnPoints)
				{
					OrcSpawner e = (OrcSpawner)world.getEntity(id);
					e.addMana(waveSize);
				}
			}

		}
		else
		{
			spawned = false;
		}

	}

	@Override
	public void initialize()
	{
		GameMap map = world.getGameMap();

		for(Point point : map.getSpawnPoints())
		{
			world.createEntity(EntityType.OrcSpawner, point.getX(), point.getY(), Direction.North);
		}
	}
}
