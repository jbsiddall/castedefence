package game;

import com.google.inject.assistedinject.Assisted;

public interface EntityFactory
{

	public Entity create
	(
		@Assisted("id") long id,
		@Assisted("x") int x,
		@Assisted("y") int y,
		@Assisted("direction") Direction direction
	);
}
