package game;

import game.entity.Treasure;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StateView
{
	private Map<Point,Set<EntityStateView>> entities;
	private Set<EntityStateView> allEntities;
	private int gold;
	private TimeStage stage;
	private int stageDuration;

	public StateView(World world)
	{
		Set<Long> worldEntities = world.getEntityIds();
		entities = new HashMap<Point, Set<EntityStateView>>(worldEntities.size());
		allEntities = new HashSet<EntityStateView>(worldEntities.size());
		gold = 0;
		stage = world.getStage();
		stageDuration = world.getStageDuration();
		for(Long id : worldEntities)
		{
			Entity entity = world.getEntity(id);

			Point loc = new Point(entity.getX(), entity.getY());
			if(!entities.containsKey(loc))
				entities.put(loc, new HashSet<EntityStateView>());

			EntityStateView view = new EntityStateView(entity);
			entities.get(loc).add(view);

			if(view.getType() == EntityType.Treasure)
			{
				gold = ((Treasure)entity).getGold();
			}

			allEntities.add(view);
		}

	}

	public EntityStateView[][] getView
	(
			final int x,
			final int y,
			final int width,
			final int height
	)
	{
		EntityStateView[][] view = new EntityStateView[height][width];
		for(int yy = y+height-1; yy >= y; yy--)
		{
			for(int xx = x; xx <= x+width-1; xx++)
			{
				int yIndex = (y+height-1) - yy;
				int xIndex = xx - x;
				view[yIndex][xIndex] = getEntityAtLocation(xx, yy);
			}
		}
		return view;
	}

	public int getGold()
	{
		return gold;
	}

	public TimeStage getStage()
	{
		return stage;
	}

	public int getStageDuration()
	{
		return stageDuration;
	}

	public EntityStateView getEntityAtLocation(int x, int y)
	{
		Point loc = new Point(x,y);
		if(entities.containsKey(loc))
		{
			Set<EntityStateView> conflicts = entities.get(loc);
			EntityStateView winner = null;
			for(EntityStateView entity : conflicts)
			{
				if(winner == null || winner.getDepth() > entity.getDepth())
				{
					winner = entity;
				}
			}
			return winner;
		}
		else
		{
			return null;
		}
	}

	public Set<EntityStateView> getEntities()
	{
		return new HashSet<EntityStateView>(allEntities);
	}


	@Override
	public boolean equals(Object o)
	{
		if(this == o) return true;
		if(!(o instanceof StateView)) return false;

		StateView stateView = (StateView) o;

		if(entities != null ? !entities.equals(stateView.entities) : stateView.entities != null) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return entities != null ? entities.hashCode() : 0;
	}

	@Override
	public String toString()
	{
		return "StateView{" +
					   "entities=" + entities +
					   '}';
	}
}
