package game.entity;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.name.Named;
import game.*;

public class Treasure extends Entity
{

	private int gold;

	@Inject
	public Treasure
	(
			@Named("gold") int gold,
			@Assisted("id") long id,
			@Assisted("x") int x,
			@Assisted("y") int y,
			@Assisted("direction") Direction direction
	)
	{
		super(id, x, y, direction, 10);
		this.gold = gold;
	}

	@Override
	public void update(WorldView world)
	{
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public String getType(){ return EntityType.Treasure; }

	@Override
	public int getDepth(){ return 1; }

	@Override
	public Team getTeam() { return Team.Player; }

	@Override
	public boolean isSolid(){ return false; }

	@Override
	public boolean isDestroyable(){ return true; }

	public void setGold(int gold)
	{
		this.gold = gold;
	}

	public int getGold()
	{
		return gold;
	}
}
