package game;

import com.google.inject.name.Named;

public interface ControllerFactory
{
	@Named("human")
	public Controller createHumanController(WorldView world);

	@Named("orc")
	public Controller createOrcController(WorldView world);
}
