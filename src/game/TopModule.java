package game;

import com.google.inject.*;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import game.entity.*;
import game.window.InteractionManager;
import sun.tools.tree.NewArrayExpression;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class TopModule extends AbstractModule
{
	private GameMap gameMap;

	public TopModule(GameMap gameMap)
	{
		this.gameMap = gameMap;
	}

	private <T extends Entity> void installEntityFactory(String name, Class<T> type)
	{
		install(new FactoryModuleBuilder()
				.implement(Entity.class, type)
				.build(Key.get(EntityFactory.class, Names.named(name)))
		);
	}

	@Override
	protected void configure()
	{
		bind(World.class).in(Singleton.class);

		install(new FactoryModuleBuilder()
				.implement(Key.get(Controller.class,Names.named("human")), HumanController.class)
				.implement(Key.get(Controller.class,Names.named("orc")), OrcController.class)
				.build(ControllerFactory.class)
		);


		installEntityFactory(EntityType.Orc, Orc.class);
		installEntityFactory(EntityType.OrcSpawner, OrcSpawner.class);
		installEntityFactory(EntityType.Treasure, Treasure.class);
		installEntityFactory(EntityType.Turret, Turret.class);
		installEntityFactory(EntityType.Wall, Wall.class);

		bind(GameMap.class).toInstance(gameMap);

		bind(Size.class).annotatedWith(Names.named("gameWindow")).toInstance(new Size(500,500));
		bind(Size.class).annotatedWith(Names.named("gameScreen")).toInstance(new Size(500,400));
		bind(Size.class).annotatedWith(Names.named("informationScreen")).toInstance(new Size(500,100));


		bind(InteractionManager.class).in(Singleton.class);


		install(new FactoryModuleBuilder().build(PathFinderFactory.class));



	}

	@Provides
	public StateView getStateView(World world)
	{
		return new StateView(world);
	}

	@Provides
	@Named("worldWidth")
	public int getWorldWidth(GameMap map)
	{
		return map.getWidth();
	}

	@Provides
	@Named("worldHeight")
	public int getWorldHeight(GameMap map)
	{
		return map.getHeight();
	}


	@Provides
	@Named("entityFactoryPool")
	public Map<String,EntityFactory> getEntityFactoryPool
	(
			@Named(EntityType.Orc) EntityFactory orc,
			@Named(EntityType.OrcSpawner) EntityFactory orcSpawner,
			@Named(EntityType.Treasure) EntityFactory treasure,
			@Named(EntityType.Turret) EntityFactory turret,
			@Named(EntityType.Wall) EntityFactory wall

	)
	{
		Map<String, EntityFactory> pool = new HashMap<String, EntityFactory>(4);
		pool.put(EntityType.Orc, orc);
		pool.put(EntityType.OrcSpawner, orcSpawner);
		pool.put(EntityType.Treasure, treasure);
		pool.put(EntityType.Turret,turret);
		pool.put(EntityType.Wall, wall);
		return pool;
	}

	@Provides
	@Named("entityTypeColors")
	public Map<String, Color> getEntityTypeColors()
	{
		Map<String, Color> typeColors = new HashMap<String, Color>();
		typeColors.put(EntityType.Orc, Color.green);
		typeColors.put(EntityType.OrcSpawner, Color.blue);
		typeColors.put(EntityType.Treasure, Color.yellow);
		typeColors.put(EntityType.Turret, Color.orange);
		typeColors.put(EntityType.Wall, Color.gray);
		return typeColors;
	}

	@Provides
	@Named("gold")
	public int getGold(GameMap map)
	{
		return map.getGold();
	}

	@Provides
	@Named("wallCost")
	public int getWallCost(GameMap map)
	{
		return map.getWallCost();
	}

	@Provides
	@Named("turretCost")
	public int getTurretCost(GameMap map)
	{
		return map.getTurretCost();
	}

	@Provides
	@Named("refundPercentage")
	public double getRefundPercentage(GameMap map)
	{
		return map.getRefundPercentage();
	}

	@Provides
	@Named("orcKillGold")
	public int getOrcKillGold(GameMap map)
	{
		return map.getOrcKillGold();
	}

}
