package game;

public class Size
{
	private int width;
	private int height;

	public Size(int width, int height)
	{
		this.width = width;
		this.height = height;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o) return true;
		if(!(o instanceof Size)) return false;

		Size size = (Size) o;

		if(height != size.height) return false;
		if(width != size.width) return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = width;
		result = 31 * result + height;
		return result;
	}

	@Override
	public String toString()
	{
		return "Size{" +
					   "width=" + width +
					   ", height=" + height +
					   '}';
	}
}
