package game;

public interface PathFinderFactory
{
	public PathFinder create(WorldView world);
}
