package game.entity;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import game.*;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Orc extends Entity
{

	private PathFinderFactory finderFactory;
	private List<Point> path;

	@Inject
	public Orc
	(
			@Assisted("id") long id,
			@Assisted("x") int x,
			@Assisted("y") int y,
			@Assisted("direction") Direction direction,
			PathFinderFactory finderFactory
	)
	{
		super(id, x, y, direction, 10);
		this.finderFactory = finderFactory;
		path = new LinkedList<Point>();
	}

	@Override
	public void update(WorldView world)
	{
		Treasure treasure = (Treasure)world.getEntity(world.getEntitiesByType(EntityType.Treasure).iterator().next());

		if(treasure.distance(getX(), getY()) <= 1d)
		{
			treasure.setGold(treasure.getGold()-1);
			world.deleteEntity(getId());
		}
		else if(path.isEmpty())
		{
			PathFinder finder = finderFactory.create(world);
			path = finder.find(getLocation(), treasure.getLocation());
		}
		else
		{
			Point step = path.remove(0);
			setLocation(step.getX(), step.getY());
		}

	}

	@Override
	public String getType() { return EntityType.Orc; }

	@Override
	public int getDepth() { return 0;}

	@Override
	public Team getTeam() { return Team.Orc; }

	@Override
	public boolean isSolid() { return false; }

	@Override
	public boolean isDestroyable() { return true; }
}
