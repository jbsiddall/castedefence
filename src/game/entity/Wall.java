package game.entity;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import game.*;

public class Wall extends Entity
{
	@Inject
	public Wall
	(
		@Assisted("id") long id,
		@Assisted("x") int x,
		@Assisted("y") int y,
		@Assisted("direction") Direction direction
	)
	{
		super(id, x, y, direction, 1000);
	}

	@Override
	public void update(WorldView world)
	{
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public String getType() { return EntityType.Wall; }

	@Override
	public int getDepth() { return 0; }

	@Override
	public Team getTeam() { return Team.Player; }

	@Override
	public boolean isSolid() { return true; }

	@Override
	public boolean isDestroyable() { return true; }
}
