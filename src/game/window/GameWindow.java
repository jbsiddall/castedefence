package game.window;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import game.Size;
import game.StateView;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameWindow extends JFrame
{
	private JPanel root;
	private GameScreen gameScreen;
	private InformationScreen informationScreen;
	private InteractionManager interactionManager;

	@Inject
	public GameWindow
	(
			@Named("gameWindow") Size windowSize,
			GameScreen gameScreen,
			InformationScreen informationScreen,
			InteractionManager interactionManager
	)
	{
		super("Castle Defence");
		setSize(windowSize.getWidth(), windowSize.getHeight());
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(600,100);


		root = new JPanel();
		this.gameScreen = gameScreen;
		this.informationScreen = informationScreen;
		this.interactionManager = interactionManager;


		addKeyListener(gameScreen);

		root.add(gameScreen);
		root.add(informationScreen);

		add(root);
	}

	public void start()
	{
		setVisible(true);
	}


	public void updateGameState(StateView gameState)
	{
		this.gameScreen.setGameState(gameState);
		this.informationScreen.setGameState(gameState);
	}

}
