package game;

import java.util.Map;
import java.util.Set;

public interface WorldView
{
	public Set<Long> getEntitiesByLocation(int x, int y);

	public Set<Long> getEntitiesByType(String type);

   	public Set<Long> getEntityIds();

	public Entity createEntity(String type, int x, int y, Direction direction);

	public Entity getEntity(Long entityId);

	public void deleteEntity(Long entityId);

	public GameMap getGameMap();

	public boolean isInsideBoard(int x, int y);

	public Set<Long> getEntitiesBySolid(boolean solid);

	public TimeStage getStage();

	public int getStageDuration();

	public Set<Long> getEntitiesByQuery(Map<String,Object> query);
	public Set<Long> getEntitiesByQuery(Object ... query);
	public Map<String,Object> createQuery(Object ... query);
}
