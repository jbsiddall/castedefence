package game;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.sun.xml.internal.ws.util.StringUtils;
import game.window.GameWindow;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

public class CastleDefence
{
	private GameMap gameMap;
	private World world;
	private GameWindow gameWindow;

	@Inject
	public CastleDefence
	(
			GameMap gameMap,
			World world,
			GameWindow gameWindow
	)
	{
		this.gameMap = gameMap;
		this.world = world;
		this.gameWindow = gameWindow;
	}

	private GameMap generateMap()
	{
		return null;
	}

	private void updateFrame()
	{
		gameWindow.updateGameState(new StateView(world));
	}

	public void start()
	{
		world.initialize();

		updateFrame();
		gameWindow.start();

		while(true)
		{
			world.update();
			StateView gameState = new StateView(world);
			gameWindow.updateGameState(gameState);
			try
			{
				SwingUtilities.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						gameWindow.repaint();
					}
				});
			}
			catch(InterruptedException e)
			{

			}
			catch(InvocationTargetException e)
			{

			}

			if(gameState.getGold() < 0)
			{
				System.out.println("GAME OVER");
				System.exit(0);
			}

			try{Thread.sleep(100);}catch(Exception e){}

		}

	}


	public static void main(String[] args)
	{
		GameMap map = new GameMap();
		Injector injector = Guice.createInjector(new TopModule(map));
		CastleDefence game = injector.getInstance(CastleDefence.class);
		game.start();
	}


}
