package game;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.name.Named;
import game.entity.Treasure;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class HumanController implements Controller
{
	private WorldView world;
	private Set<Point> createWallLocations;
	private Set<Point> sellWallLocations;
	private Set<Point> createTurretLocations;
	private Set<Point> sellTurretLocations;

	private int wallCost;
	private int turretCost;
	private double refundPercentage;

	@Inject
	public HumanController
	(
			@Named("wallCost") int wallCost,
			@Named("turretCost") int turretCost,
			@Named("refundPercentage") double refundPercentage,
			@Assisted WorldView world
	)
	{
		this.wallCost = wallCost;
		this.turretCost = turretCost;
		this.refundPercentage = refundPercentage;
		this.world = world;

		createWallLocations = new HashSet<Point>();
		sellWallLocations = new HashSet<Point>();
		createTurretLocations = new HashSet<Point>();
		sellTurretLocations = new HashSet<Point>();
	}

	public void update()
	{
		Treasure treasure = (Treasure)world.getEntity(world.getEntitiesByType(EntityType.Treasure).iterator().next());
		int gold = treasure.getGold();

		// sell wall
		for(Point sellWallLocation : sellWallLocations)
		{
			int x = sellWallLocation.getX();
			int y = sellWallLocation.getY();
			Set<Long> results = world.getEntitiesByLocation(x,y);
			results.retainAll(world.getEntitiesByType(EntityType.Wall));

			if(!results.isEmpty())
			{
				treasure.setGold(treasure.getGold() + (int)(wallCost * refundPercentage));
				Long id = results.iterator().next();
				world.deleteEntity(id);
			}
		}
		sellWallLocations.clear();

		// sell turrets
		for(Point sellTurretLocation : sellTurretLocations)
		{
			int x = sellTurretLocation.getX();
			int y = sellTurretLocation.getY();
			Set<Long> results = world.getEntitiesByLocation(x,y);
			results.retainAll(world.getEntitiesByType(EntityType.Turret));

			if(!results.isEmpty())
			{
				treasure.setGold(treasure.getGold() + (int)(turretCost * refundPercentage));
				Long id = results.iterator().next();
				world.deleteEntity(id);
			}
		}
		sellTurretLocations.clear();

		// build wall
		for(Point createWallLocation : createWallLocations)
		{
			int x = createWallLocation.getX();
			int y = createWallLocation.getY();
			if((gold >= wallCost) && world.getEntitiesByLocation(x,y).isEmpty())
			{
				treasure.setGold(gold - wallCost);
				world.createEntity(EntityType.Wall, x, y, Direction.North);
			}
		}
		createWallLocations.clear();

		// build turret
	   	for(Point createTurretLocation : createTurretLocations)
		{
			int x = createTurretLocation.getX();
			int y = createTurretLocation.getY();
			if((gold >= turretCost) && world.getEntitiesByLocation(x,y).isEmpty())
			{
				treasure.setGold(gold - turretCost);
				world.createEntity(EntityType.Turret, x, y, Direction.North);
			}
		}
		createTurretLocations.clear();
	}

	public void createWallAtPoint(Point location)
	{
		createWallLocations.add(location);
	}

	public void deleteWallAtPoint(Point location)
	{
		sellWallLocations.add(location);
	}

	public void createTurretAtPoint(Point location)
	{
		createTurretLocations.add(location);
	}

	public void deleteTurretAtPoint(Point location)
	{
		sellTurretLocations.add(location);
	}


	@Override
	public void initialize()
	{
		GameMap map = world.getGameMap();
		Point treasure = map.getTreasurePoint();

		world.createEntity(EntityType.Treasure, treasure.getX(), treasure.getY(), Direction.North);
	}
}
