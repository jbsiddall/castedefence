package game;

import java.util.HashSet;
import java.util.Set;

public class GameMap
{
	private int width;
	private int height;

	private int gold;
	private int wallCost;
	private int turretCost;
	private double refundPercentage;
	private int orcKillGold;

	private Point treasurePoint;
	private Set<Point> spawnPoints;

	public static GameMap LARGE()
	{
		GameMap map = new GameMap();
		map.setWidth(31);
		map.setHeight(31);
		map.setGold(200);
		map.setTreasurePoint(16,16);
		map.addSpawnPoint(1,16);
		map.addSpawnPoint(31,16);
		map.addSpawnPoint(16,1);
		map.addSpawnPoint(16,31);

		return map;
	}

	public GameMap()
	{
		width = 10;
		height = 10;
		gold = 1000;
		wallCost = 10;
		turretCost = 30;
		refundPercentage = 0.6;
		orcKillGold = 1;

		treasurePoint = new Point(5, 5);
		spawnPoints = new HashSet<Point>();
		spawnPoints.add(new Point(5,10));
	}


	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	public int getGold()
	{
		return gold;
	}

	public void setGold(int gold)
	{
		this.gold = gold;
	}

	public int getWallCost()
	{
		return wallCost;
	}

	public void setWallCost(int wallCost)
	{
		this.wallCost = wallCost;
	}

	public int getTurretCost()
	{
		return turretCost;
	}

	public void setTurretCost(int turretCost)
	{
		this.turretCost = turretCost;
	}

	public double getRefundPercentage()
	{
		return refundPercentage;
	}

	public void setRefundPercentage(double refundPercentage)
	{
		this.refundPercentage = refundPercentage;
	}

	public Point getTreasurePoint()
	{
		return treasurePoint;
	}

	public void setTreasurePoint(int x, int y)
	{
		setTreasurePoint(new Point(x,y));
	}

	public void setTreasurePoint(Point treasurePoint)
	{
		this.treasurePoint = treasurePoint;
	}

	public Set<Point> getSpawnPoints()
	{
		return spawnPoints;
	}

	public void addSpawnPoint(int x, int y)
	{
		addSpawnPoint(new Point(x,y));
	}

	public void addSpawnPoint(Point p)
	{
		if(spawnPoints == null)
			spawnPoints = new HashSet<Point>();
		spawnPoints.add(p);
	}

	public void setSpawnPoints(Point... points)
	{
		Set<Point> sp = new HashSet<Point>();
		for(Point p : points)
			sp.add(p);
		setSpawnPoints(sp);
	}

	public void setSpawnPoints(Set<Point> spawnPoints)
	{
		this.spawnPoints = spawnPoints;
	}

	public int getOrcKillGold()
	{
		return orcKillGold;
	}

	public void setOrcKillGold(int orcKillGold)
	{
		this.orcKillGold = orcKillGold;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o) return true;
		if(!(o instanceof GameMap)) return false;

		GameMap gameMap = (GameMap) o;

		if(gold != gameMap.gold) return false;
		if(height != gameMap.height) return false;
		if(Double.compare(gameMap.refundPercentage, refundPercentage) != 0) return false;
		if(turretCost != gameMap.turretCost) return false;
		if(wallCost != gameMap.wallCost) return false;
		if(width != gameMap.width) return false;
		if(spawnPoints != null ? !spawnPoints.equals(gameMap.spawnPoints) : gameMap.spawnPoints != null) return false;
		if(treasurePoint != null ? !treasurePoint.equals(gameMap.treasurePoint) : gameMap.treasurePoint != null)
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result;
		long temp;
		result = width;
		result = 31 * result + height;
		result = 31 * result + gold;
		result = 31 * result + wallCost;
		result = 31 * result + turretCost;
		temp = refundPercentage != +0.0d ? Double.doubleToLongBits(refundPercentage) : 0L;
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (treasurePoint != null ? treasurePoint.hashCode() : 0);
		result = 31 * result + (spawnPoints != null ? spawnPoints.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return "GameMap{" +
					   "width=" + width +
					   ", height=" + height +
					   ", gold=" + gold +
					   ", wallCost=" + wallCost +
					   ", turretCost=" + turretCost +
					   ", refundPercentage=" + refundPercentage +
					   ", treasurePoint=" + treasurePoint +
					   ", spawnPoints=" + spawnPoints +
					   '}';
	}
}
