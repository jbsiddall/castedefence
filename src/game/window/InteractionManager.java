package game.window;

import com.google.inject.Inject;
import game.HumanController;
import game.Point;
import game.TimeStage;
import game.World;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InteractionManager
{
	private World world;

	public static enum BuildingSelected { Wall, Turret, Nothing }

	BuildingSelected buildingSelected;

	@Inject
	public InteractionManager(World world)
	{
		this.world = world;
	}

	public void worldClick(Point location, boolean button1)
	{
		HumanController controller = ((HumanController)world.getHumanController());
		if(BuildingSelected.Wall == buildingSelected)
		{
			if(button1) controller.createWallAtPoint(location);
			else controller.deleteWallAtPoint(location);
		}
		else if(BuildingSelected.Turret == buildingSelected)
		{
			if(button1) controller.createTurretAtPoint(location);
			else controller.deleteTurretAtPoint(location);
		}
	}

    public void skipWaitTime()
    {
        if(world.getStage() == TimeStage.Build) {
            world.setStageDuration(1);
        }
    }

	public BuildingSelected getBuildingSelected()
	{
		return buildingSelected;
	}

	public void buildWallSelected(boolean selected)
	{
		if(selected)
		{
			buildingSelected = BuildingSelected.Wall;
		}
		else if(buildingSelected == BuildingSelected.Wall)
		{
			buildingSelected = BuildingSelected.Nothing;
		}
	}

	public void buildTurretSelected(boolean selected)
	{
		if(selected)
		{
			buildingSelected = BuildingSelected.Turret;
		}
		else if(buildingSelected == BuildingSelected.Turret)
		{
			buildingSelected = BuildingSelected.Nothing;
		}

	}


}
