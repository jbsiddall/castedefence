package game.entity;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import game.*;

import java.util.Set;

public class Turret extends Entity
{
	private int damageGiven;
	private int range;

	@Inject
	public Turret
	(
			@Assisted("id") long id,
			@Assisted("x") int x,
			@Assisted("y") int y,
			@Assisted("direction") Direction direction
	)
	{
		super(id, x, y, direction, 50);
		damageGiven = 1;
		range = 3;
	}

	private double distance(int x1, int y1, int x2, int y2)
	{
		return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
	}

	@Override
	public void update(WorldView world)
	{
		Set<Long> validTargets = world.getEntitiesByType(EntityType.Orc);
		Entity closest = null;
		double dist = 0;
		for(Long id : validTargets)
		{
			Entity e = world.getEntity(id);
			if(closest == null)
			{
				closest = e;
				dist = distance(getX(), getY(), e.getX(), e.getY());
			}
			else
			{
				double tmp = distance(getX(),getY(), e.getX(), e.getY());
				if(tmp < dist)
				{
					dist = tmp;
					closest = e;
				}
			}
		}

		if(closest != null && dist <= range)
		{
			closest.damage(damageGiven);

			if(!closest.isAlive())
			{
				Treasure treasure = (Treasure)world.getEntity(world.getEntitiesByType(EntityType.Treasure).iterator().next());
                treasure.setGold(treasure.getGold() + 10);
			}
		}

	}

	@Override
	public String getType() { return EntityType.Turret; }

	@Override
	public int getDepth() { return 0; }

	@Override
	public Team getTeam() { return Team.Player; }

	@Override
	public boolean isSolid() { return true; }

	@Override
	public boolean isDestroyable() { return true; }
}
