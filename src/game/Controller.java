package game;

public interface Controller
{
	public void initialize();

	public void update();
}
