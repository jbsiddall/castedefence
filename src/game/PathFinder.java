package game;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.name.Named;
import org.newdawn.slick.util.pathfinding.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class PathFinder implements TileBasedMap, AStarHeuristic
{
	private int wallCost;
	private int turretCost;
	private WorldView world;
	private float healthMultiplier;

	@Inject
	public PathFinder
	(
			@Named("wallCost") int wallCost,
			@Named("turretCost") int turretCost,
			@Assisted WorldView world
	)
	{
		this.wallCost = wallCost;
		this.turretCost = turretCost;
		this.world = world;
		healthMultiplier = 1;
	}

	public List<Point> find(Point start, Point finish)
	{
		int maxPathLength = world.getGameMap().getWidth()*world.getGameMap().getHeight();
		AStarPathFinder finder = new AStarPathFinder(this, maxPathLength, false, this);

		Path path = finder.findPath(null, start.getX(), start.getY(), finish.getX(), finish.getY());

		List<Point> list = new LinkedList<Point>();
		for(int stepI = 0; stepI < path.getLength(); stepI++)
		{
			Path.Step step = path.getStep(stepI);
			list.add(new Point(step.getX(), step.getY()));
		}
		return list;
	}

	@Override
	public int getWidthInTiles()
	{
		return world.getGameMap().getWidth()+1;
	}

	@Override
	public int getHeightInTiles()
	{
		return world.getGameMap().getHeight()+1;
	}

	@Override
	public void pathFinderVisited(int x, int y)
	{

	}

	@Override
	public boolean blocked(PathFindingContext context, int tx, int ty)
	{
		if(!world.isInsideBoard(tx,ty)) return true;
		Set<Long> results = world.getEntitiesByLocation(tx, ty);
		results.retainAll(world.getEntitiesBySolid(true));
		return !results.isEmpty();
	}

	@Override
	public float getCost(PathFindingContext context, int tx, int ty)
	{
		Set<Long> atLocation = world.getEntitiesByLocation(tx, ty);

		Set<Long> turrets = world.getEntitiesByType(EntityType.Turret);
		Set<Long> walls = world.getEntitiesByType(EntityType.Wall);

		turrets.retainAll(atLocation);
		walls.retainAll(atLocation);

		Set<Long> items = turrets;
		items.addAll(walls);

		float health = 0;

		for(Long id : items)
		{
			Entity e = world.getEntity(id);
			if(e.getHealth() > health)
				health = e.getHealth();
		}

		return health*healthMultiplier;
	}

	@Override
	public float getCost(TileBasedMap map, Mover mover, int x, int y, int tx, int ty)
	{
		return (float)Math.sqrt( Math.pow(tx-x,2) + Math.pow(ty-y,2) );
	}
}
