package game;

public class EntityType
{
	public static final String Orc = "orc";
	public static final String OrcSpawner = "orcspawner";
	public static final String Treasure = "treasure";
	public static final String Turret = "turret";
	public static final String Wall = "wall";

}
