package game.window;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import game.Size;
import game.StateView;
import game.TimeStage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InformationScreen extends JPanel implements ActionListener
{
	private InteractionManager interactionManager;

	private JPanel statsPanel;
	private JLabel goldStatsLabel;
	private JLabel refundLabel;
	private JLabel timeStage;

	private JPanel buildPanel;
	private JToggleButton buildWallButton;
	private JToggleButton buildTurretButton;
    private JToggleButton skipButton;

	private StateView gameState;

	private int wallCost;
	private int turretCost;
	private double refundPercentage;

	@Inject
	public InformationScreen
	(
			@Named("informationScreen") Size informationScreenSize,
			@Named("wallCost") int wallCost,
			@Named("turretCost") int turretCost,
			@Named("refundPercentage") double refundPercentage,
			InteractionManager interactionManager,
			StateView initialGameState
	)
	{
		super();
		setPreferredSize(new Dimension(informationScreenSize.getWidth(), informationScreenSize.getHeight()));

		this.interactionManager = interactionManager;
		this.gameState = initialGameState;
		this.wallCost = wallCost;
		this.turretCost = turretCost;
		this.refundPercentage = refundPercentage;

		this.statsPanel = new JPanel();
		BoxLayout layout = new BoxLayout(this.statsPanel, BoxLayout.Y_AXIS);
		this.statsPanel.setLayout(layout);
		statsPanel.setBorder(BorderFactory.createCompoundBorder());
		add(this.statsPanel);

		this.goldStatsLabel = new JLabel();
		this.statsPanel.add(goldStatsLabel);

		this.refundLabel = new JLabel();
		this.statsPanel.add(refundLabel);

		this.timeStage = new JLabel();
		this.statsPanel.add(timeStage);


		this.buildPanel = new JPanel();
		add(this.buildPanel);

		this.buildWallButton = new JToggleButton(String.format("WALL: %d", wallCost));
		this.buildWallButton.setName("buildWallButton");
		this.buildPanel.add(this.buildWallButton);
		this.buildWallButton.addActionListener(this);

		this.buildTurretButton = new JToggleButton(String.format("TURRET: %d", turretCost));
		this.buildTurretButton.setName("buildTurretButton");
		this.buildPanel.add(this.buildTurretButton);
		this.buildTurretButton.addActionListener(this);

        this.skipButton = new JToggleButton("SKIP");
        this.skipButton.setName("skipButton");
        this.buildPanel.add(this.skipButton);
        this.skipButton.addActionListener(this);

		this.updateInformation();
	}

	public void updateInformation()
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				int gold = gameState.getGold();
				goldStatsLabel.setText(String.format("gold: %d", gold));

				refundLabel.setText(String.format("refund: %1.0f%%", refundPercentage*100));

				timeStage.setText(String.format("stage: %s, remaining: %d", gameState.getStage(), gameState.getStageDuration()));

				/*
							private JLabel refundLabel;
					private JLabel timeStage;
						 */

				buildWallButton.setSelected(InteractionManager.BuildingSelected.Wall == interactionManager.getBuildingSelected());
				buildTurretButton.setSelected(InteractionManager.BuildingSelected.Turret == interactionManager.getBuildingSelected());
			}
		});

	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String txt = e.getActionCommand();
		if(this.buildWallButton.getText().equals(txt))
		{
			interactionManager.buildWallSelected(buildWallButton.isSelected());
		}
		else if(this.buildTurretButton.getText().equals(txt))
		{
			interactionManager.buildTurretSelected(buildTurretButton.isSelected());
		} else if(this.skipButton.getText().equals(txt))
        {
            if(gameState.getStage() == TimeStage.Build) {
                interactionManager.skipWaitTime();
            }
        }
		this.updateInformation();
	}

	public StateView getGameState()
	{
		return gameState;
	}

	public void setGameState(StateView gameState)
	{
		this.gameState = gameState;

		buildPanel.setVisible(gameState.getStage() == TimeStage.Build);

		this.updateInformation();
	}
}

