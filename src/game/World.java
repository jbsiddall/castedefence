package game;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import game.entity.Wall;
import org.omg.CORBA.SystemException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class World implements WorldView
{

	private GameMap gameMap;

	private long nextId;
	private Map<Long, Entity> entities;
	private Set<Entity> addCache;
	private Set<Long> deleteCache;

	private Map<String, EntityFactory> entityFactoryPool;

	private Controller humanController;
	private Controller orcController;
	private ControllerFactory controllerFactory;

	private Set<String> searchCafeterias;

	private boolean isInitialized;

	private TimeStage stage;
	private int BUILD_DURATION = 300;
	private int buildDuration;

	private int DEFEND_DURATION = 20;
	private int defendDuration;


	public boolean isInsideBoard(int x, int y)
	{
		return ((1 <= x && x <= gameMap.getWidth()) && (1 <= y && y <= gameMap.getHeight()));
	}

	@Inject
	public World
	(
			GameMap gameMap,
			@Named("entityFactoryPool") Map<String, EntityFactory> entityFactoryPool,
			ControllerFactory controllerFactory
	)
	{
		this.gameMap = gameMap;
		this.stage = TimeStage.Build;
		this.buildDuration = BUILD_DURATION;
		this.defendDuration = DEFEND_DURATION;

		this.nextId = 0l;
		this.entityFactoryPool = entityFactoryPool;

		this.entities = new HashMap<Long, Entity>(1000);
		this.addCache = new HashSet<Entity>(1000);
		this.deleteCache = new HashSet<Long>(1000);

		this.controllerFactory = controllerFactory;
		this.isInitialized = false;

		Set<String> searchCafeterias = new HashSet<String>();
		searchCafeterias.add(Query.Location);
		searchCafeterias.add(Query.Id);
		searchCafeterias.add(Query.Solid);
		searchCafeterias.add(Query.Type);
		this.searchCafeterias = searchCafeterias;
	}

	public void initialize()
	{
		this.isInitialized = true;
		this.humanController = controllerFactory.createHumanController(this);
		this.orcController = controllerFactory.createOrcController(this);
		this.humanController.initialize();
		this.orcController.initialize();
		this.flush();
	}

	public void update()
	{
		if(!isInitialized)
			this.initialize();

		if(stage == TimeStage.Build)
		{
			if(buildDuration == 0)
			{
				stage = TimeStage.Defend;
				defendDuration = DEFEND_DURATION;
			}
			else
				buildDuration--;
		}
		else if(stage == TimeStage.Defend)
		{
			if(defendDuration == 0)
			{
				if(getEntitiesByType(EntityType.Orc).isEmpty())
				{
					stage = TimeStage.Build;
					buildDuration = BUILD_DURATION;
				}
			}
			else
			{
				defendDuration--;
			}

		}


		this.humanController.update();
		this.orcController.update();

		for(Entity entity : entities.values())
		{
			entity.update((WorldView)this);
		}

		for(Entity entity : entities.values())
		{
			if(!entity.isAlive() && entity.isDestroyable())
				deleteEntity(entity.getId());
		}

		flush();
	}

	private void flush()
	{
		for(Entity entity : addCache)
		{
			entities.put(entity.getId(), entity);
		}
		addCache.clear();

		for(Long id : deleteCache)
		{
			entities.remove(id);
		}
		deleteCache.clear();
		Runtime.getRuntime().gc();
	}

	public Entity createEntity(String type, int x, int y, Direction direction)
	{
		if(isInsideBoard(x, y))
		{
			Entity entity = entityFactoryPool.get(type).create(nextId++, x, y, direction);
			this.addCache.add(entity);
			return entity;
		}
		else
		{
			System.err.printf("(%d, %d) is outside of world\n", x, y);
			System.exit(1);
			return null;
		}

	}

	public Entity getEntity(Long entityId)
	{
		return entities.get(entityId);
	}

	public void deleteEntity(Long entityId)
	{
		this.deleteCache.add(entityId);
	}


	public GameMap getGameMap()
	{
		return gameMap;
	}



	@Override
	public Set<Long> getEntitiesByLocation(int x, int y)
	{
		Set<Long> results = new HashSet<Long>();
		for(Entity entity : entities.values())
		{
			if(entity.getX() == x && entity.getY() == y)
				results.add(entity.getId());
		}
		return results;
	}

	@Override
	public Set<Long> getEntitiesByType(String type)
	{
		Set<Long> results = new HashSet<Long>();
		for(Entity entity : entities.values())
		{
			if(entity.getType().equals(type))
				results.add(entity.getId());
		}
		return results;
	}

	public Set<Long> getEntitiesBySolid(boolean solid)
	{
		Set<Long> results = new HashSet<Long>();
		for(Entity entity : entities.values())
		{
			if(entity.isSolid() == solid)
			{
				results.add(entity.getId());
			}
		}
		return results;
	}

	@Override
	public Set<Long> getEntityIds()
	{
		return new HashSet<Long>(entities.keySet());
	}

	public Map<String,Object> createQuery(Object ... query)
	{
		if((query.length/2)*2 != query.length)
		{
			System.err.println("odd number of arguments");
			System.exit(1);
		}

		Map<String, Object> criteria = new HashMap<String, Object>();
		for(int index = 0; index < query.length; index+=2)
		{
			String key = (String)query[index];
			Object value = query[index+1];
			criteria.put(key, value);
		}
		return criteria;
	}

	public Set<Long> getEntitiesByQuery(Object ... query)
	{
		return getEntitiesByQuery(createQuery(query));
	}

	public Set<Long> getEntitiesByQuery(Map<String,Object> query)
	{
		Set<String> unknown = new HashSet<String>(query.keySet());
		unknown.removeAll(searchCafeterias);
		if(!unknown.isEmpty())
		{
			System.err.printf("unknown search criteria: %s\n", unknown);
			System.exit(1);
		}

		Set<String> keys = query.keySet();
		Set<Long> results = new HashSet<Long>();
		for(Entity e : entities.values())
		{
			int required = keys.size();
			if(keys.contains(Query.Id) && e.getId()==((Long)query.get(Query.Id)).longValue())
			{
				required--;
			}
			if(keys.contains(Query.Solid) && e.isSolid()==((Boolean)query.get(Query.Solid)).booleanValue())
			{
				required--;
			}
			if(keys.contains(Query.Type) && e.getType().equals( (String)query.get(Query.Type) ) )
			{
				required--;
			}
			if(keys.contains(Query.Location) && e.getLocation().equals( (Point)query.get(Query.Location) ))
			{
				required--;
			}

			if(required==0)
			{
				results.add(e.getId());
			}
		}

		return results;

	}

	public Controller getOrcController()
	{
		return orcController;
	}

	public Controller getHumanController()
	{
		return humanController;
	}

	public TimeStage getStage()
	{
		return stage;
	}

	public int getStageDuration()
	{
		if(stage == TimeStage.Build) return buildDuration;
		else return defendDuration;
	}

    public void setStageDuration(int duration)
    {
        if(stage == TimeStage.Build) buildDuration = duration;
        else defendDuration = duration;
    }

}
